##
# Run Docker-Container:
##
sudo docker run --runtime=nvidia --rm nvidia/cuda -it -v /srv/docker/projektS6/worcspace/:/srv/ -p 8888:8888 tensorflow/tensorflow:latest-gpu
#or
sudo docker run -it -v /srv/docker/projektS6/worcspace/:/srv/ -p 8888:8888 tensorflow/tensorflow:latest
#or
sudo docker run --runtime=nvidia -it -v /srv/Workspace/HS/Projekt/worcspace/:/srv/ -p 8888:8888 tensorflow/tensorflow:latest-gpu
#or
sudo docker run --runtime=nvidia -it -v /srv/Workspace/HS/Projekt/worcspace/:/srv/ -p 8888:8888 tensorflow/tensorflow:1.13.1-gpu

##
# Run training in the Docker-Container:
##
python ./Code/train.py --data_url=  --data_dir=/srv/Data/ --wanted_words=start,stop,weiter,zurück --how_many_training_steps=15001,3000
#or
python ./Code/train.py --data_url=  --data_dir=/srv/Data/ --wanted_words=start,stop,weiter,zurueck,pause,musik --how_many_training_steps=3500,800 
#or
python ./Code/train.py --data_url=  --data_dir=/srv/Data/ --wanted_words=start,stop,weiter,zurueck,pause,musik --how_many_training_steps=13333,3333 1>Log/TrainLog.txt 2>&1

##
# Watch GPU usage
##
watch nvidia-smi
#or CPU
htop

##
# Convert with script from example to model for mobile app
##
python speech_commands/freeze.py --start_checkpoint=Train/conv.ckpt-5500 --output_file=Model/frozenModel.pb --wanted_words=start,stop,weiter,zurueck,pause,musik

##
# Test Model
##
python speech_commands/label_wav.py --graph=Model/frozenModel_train3.pb --labels=Train/conv_labels.txt --wav=Data/Stop/aaron_nohash_002.wav

##
# other
##
mplayer -identify filename
python -c 'import tensorflow as tf; print(tf.__version__)'
sudo du -sch .[.*]* /*
