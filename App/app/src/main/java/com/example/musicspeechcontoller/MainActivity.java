package com.example.musicspeechcontoller;

import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import org.w3c.dom.Text;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    private MusicPlayer mp;
    private AudioManager audioManager;
    private ImageButton playButton;
    private ImageButton pauseButton;
    private ImageButton stopButton;
    private ImageButton forwardButton;
    private ImageButton backwardButton;
    private ImageButton muteButton;
    private ImageButton keywordButton;
    private ImageView muteBackground;
    private ImageView keywordBackground;
    private ImageView androidBootIcon;
    private ImageView androidBootHead;
    private TextView songTitle;

    SpeechRecognition speechRecognition;

    private static final int REQUEST_RECORD_AUDIO = 13;
    private static final int LISTEN_DURATION_MS = 3000;
    private long listenTimestamp = 0;

    private boolean useSound = true;
    private boolean useKeyword = false;

    private static final String LOG_TAG = MainActivity.class.getSimpleName();



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        songTitle = findViewById(R.id.songTitle);

        androidBootIcon = findViewById(R.id.android_boot_icon);
        androidBootHead = findViewById(R.id.android_boot_head);
        mp = new MusicPlayer(this);
        songTitle.setText(mp.getSongTitle());
        audioManager = (AudioManager)getSystemService(this.AUDIO_SERVICE);

        playButton = findViewById(R.id.btn_start);
        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                play();
            }
        });

        pauseButton = findViewById(R.id.btn_pause);
        pauseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
               pause();
            }
        });

        stopButton = findViewById(R.id.btn_stop);
        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                stop();
            }
        });

        forwardButton = findViewById(R.id.btn_forward);
        forwardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
               forward();
            }
        });

        backwardButton = findViewById(R.id.btn_backward);
        backwardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
               backward();
            }
        });

        keywordBackground = findViewById(R.id.background_keyword);
        keywordButton = findViewById(R.id.btn_keyword);
        keywordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                useKeyword = !useKeyword;
                setKeyword(useKeyword);
                Log.d(LOG_TAG, "Use Keyword: " + useKeyword);
            }
        });

        muteBackground = findViewById(R.id.background_mute);
        muteButton = findViewById(R.id.btn_mute);
        muteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                setMute(useSound);
                useSound = !useSound;
            }
        });

        //RecognizedWordListener
        speechRecognition = new SpeechRecognition(this);
        //Setup the listener for this object
        speechRecognition.setRecognizedWordListener(new SpeechRecognition.RecognizedWordListener() {

            @Override
            public void onWordRecognized(String recognizedWord) {
                switch (recognizedWord){
                    case "start":
                        Log.d(LOG_TAG, "Command: start");
                        if(!useKeyword || isInListeningDuration()){
                            play();
                        }
                        break;
                    case "pause":
                        Log.d(LOG_TAG, "Command: pause");
                        if(!useKeyword || isInListeningDuration()){
                            pause();
                        }
                        break;
                    case "stop":
                        Log.d(LOG_TAG, "Command: stop");
                        if(!useKeyword || isInListeningDuration()) {
                            stop();
                        }
                        break;
                    case "weiter":
                        Log.d(LOG_TAG, "Command: weiter");
                        if(!useKeyword || isInListeningDuration()) {
                            forward();
                        }
                        break;
                    case "zurueck":
                        Log.d(LOG_TAG, "Command: zurueck");
                        if(!useKeyword || isInListeningDuration()) {
                            backward();
                        }
                        break;
                    case "musik":
                        Log.d(LOG_TAG, "Command: musik");
                        if(useKeyword && !isInListeningDuration()) {
                            listenTimestamp = System.currentTimeMillis();
                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    androidBootHead.setVisibility(View.VISIBLE);
                                }
                            });
                            new Timer().schedule(new TimerTask() {
                                @Override
                                public void run() {
                                    runOnUiThread(new Runnable() {

                                        @Override
                                        public void run() {
                                            androidBootHead.setVisibility(View.INVISIBLE);
                                        }
                                    });
                                }
                            }, LISTEN_DURATION_MS);
                            Log.d(LOG_TAG, "Is listening.");
                        }
                        break;
                }
            }
        });

        Log.d(LOG_TAG, "Use Keyword: " + useKeyword);

        requestMicrophonePermission();
        setKeyword(useKeyword);
        setMute(useSound);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void setMute(Boolean useSound){
        if(useSound){
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    muteButton.setImageResource(R.drawable.ic_not_allowed_white_100dp);
                    muteBackground.setImageResource(R.drawable.ic_volume_up_light_gray_100dp);
                    audioManager.setStreamMute(AudioManager.STREAM_MUSIC, true);
                }
            });
        } else {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    muteButton.setImageDrawable(null);
                    muteBackground.setImageResource(R.drawable.ic_volume_up_white_100dp);
                    audioManager.setStreamMute(AudioManager.STREAM_MUSIC, false);
                }
            });
        }
    }

    private void setKeyword(Boolean useKeyword){
        if(useKeyword){
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    keywordButton.setImageDrawable(null);
                    keywordBackground.setImageResource(R.drawable.ic_android_white_300dp);
                }
            });
        } else {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    keywordButton.setImageResource(R.drawable.ic_not_allowed_white_100dp);
                    keywordBackground.setImageResource(R.drawable.ic_android_light_gray_300dp);
                }
            });
        }
    }

    private void play(){
        mp.play();
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                androidBootIcon.setImageResource(R.drawable.ic_play_arrow_color_background_100dp);
            }
        });
    }

    private void stop(){
        mp.stop();
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                androidBootIcon.setImageResource(R.drawable.ic_stop_color_background_100dp);
            }
        });
    }

    private void pause(){
        if(mp.pause()){
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    androidBootIcon.setImageResource(R.drawable.ic_pause_color_background_100dp);
                }
            });
        } else {
            final Drawable imageBefore = androidBootIcon.getDrawable();
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    androidBootIcon.setImageResource(R.drawable.ic_not_allowed_color_background_100dp);
                }
            });
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            androidBootIcon.setImageDrawable(imageBefore);
                        }
                    });
                }
            }, 250);
        }
    }

    private void forward(){
        mp.forward();
        songTitle.setText(mp.getSongTitle());
        final Drawable imageBefore = androidBootIcon.getDrawable();
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                androidBootIcon.setImageResource(R.drawable.ic_skip_next_color_background_100dp);
            }
        });
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        androidBootIcon.setImageDrawable(imageBefore);
                    }
                });
            }
        }, 250);
    }

    private void backward(){
        mp.backward();
        songTitle.setText(mp.getSongTitle());
        final Drawable imageBefore = androidBootIcon.getDrawable();
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                androidBootIcon.setImageResource(R.drawable.ic_skip_previous_color_background_100dp);
            }
        });
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        androidBootIcon.setImageDrawable(imageBefore);
                    }
                });
            }
        }, 250);
    }

    private void requestMicrophonePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(
                    new String[]{android.Manifest.permission.RECORD_AUDIO}, REQUEST_RECORD_AUDIO);
        }
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_RECORD_AUDIO
                && grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            speechRecognition.startRecording();
            speechRecognition.startRecognition();
        }
    }

    private boolean isInListeningDuration(){
        return (System.currentTimeMillis() - listenTimestamp < LISTEN_DURATION_MS);
    }
}

