package com.example.musicspeechcontoller;

import android.content.Context;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.Log;

import java.util.ArrayList;

public class MusicPlayer {
    private ArrayList<Integer> playlist;
    private ArrayList<String> playlistTitle;
    private int currentSongIndex = 0;
    private MediaPlayer mediaPlayer;
    private Context context;
    private MediaMetadataRetriever mediaMetadata;

    private Boolean inProcessLook = false;

    private static final String LOG_TAG = MusicPlayer.class.getSimpleName();

    public MusicPlayer(Context context){
        this.context = context;
        playlist = new ArrayList<>();
        playlist.add(R.raw.dontgimmethat);
        playlist.add(R.raw.hayday);
        playlist.add(R.raw.ikeepondancing);
        playlist.add(R.raw.killers);
        playlist.add(R.raw.libertyofaction);
        playlist.add(R.raw.liveitup);
        playlist.add(R.raw.love);
        playlist.add(R.raw.moneythatswhatiwant);
        playlist.add(R.raw.mycountry);
        playlist.add(R.raw.myway);
        playlist.add(R.raw.ridinghighsinginglow);
        playlist.add(R.raw.runrundevil);
        playlist.add(R.raw.sexonlegs);
        playlist.add(R.raw.stillcrazyboutelvis);
        playlist.add(R.raw.theanswer);

        playlistTitle = new ArrayList<>();
        playlistTitle.add("Don't Gimme That");
        playlistTitle.add("Hay Day");
        playlistTitle.add("I Keep On Dancing");
        playlistTitle.add("Killers");
        playlistTitle.add("Liberty Of Action");
        playlistTitle.add("Live It Up");
        playlistTitle.add("L.O.V.E.");
        playlistTitle.add("Money That's All I Want");
        playlistTitle.add("My Country");
        playlistTitle.add("My Way");
        playlistTitle.add("Riding High Singing Low");
        playlistTitle.add("Run Run Devil");
        playlistTitle.add("Sex On Legs");
        playlistTitle.add("Still Crazy 'bout Elvis");
        playlistTitle.add("The Answer");

        mediaPlayer = MediaPlayer.create(context, playlist.get(currentSongIndex));
    }

    public String getSongTitle(){
        return playlistTitle.get(currentSongIndex);
    }

    public void play(){

        if(!inProcessLook) {
            inProcessLook = !inProcessLook;
            hardPlay();
            inProcessLook = !inProcessLook;
        }
    }

    private void hardPlay() {
        if(mediaPlayer != null) {
            mediaPlayer.start();
        }
    }

    public boolean pause(){
        boolean result = true;
        if(!inProcessLook) {
            inProcessLook = !inProcessLook;
            if(mediaPlayer != null) {
                if(mediaPlayer.isPlaying()){
                    mediaPlayer.pause();
                } else {
                    result = false;
                }
            }
            inProcessLook = !inProcessLook;
        }
        return result;
    }

    public void stop(){

        if(!inProcessLook) {
            inProcessLook = !inProcessLook;
            hardStop();
            inProcessLook = !inProcessLook;
        }
    }

    private void hardStop(){
        if(mediaPlayer != null) {
            mediaPlayer.stop();
            try {
                mediaPlayer.prepare();
            } catch (Exception e) {

            }
        }
    }

    public void forward(){
        if(!inProcessLook) {
            inProcessLook = !inProcessLook;
            if(mediaPlayer != null) {
                Boolean wasPlaying = mediaPlayer.isPlaying();
                if (++currentSongIndex >= playlist.size()) {
                    currentSongIndex = 0;
                }

                if (wasPlaying) {
                    hardStop();
                }
                mediaPlayer.release();
                mediaPlayer = MediaPlayer.create(context, playlist.get(currentSongIndex));
                if (wasPlaying) {
                    hardPlay();
                }
            }
            inProcessLook = !inProcessLook;
        }
    }

    public void backward(){
        if(!inProcessLook) {
            inProcessLook = !inProcessLook;
            if(mediaPlayer != null) {
                if (mediaPlayer.isPlaying()) {
                    hardStop();
                    hardPlay();
                } else {
                    if (--currentSongIndex < 0) {
                        currentSongIndex = playlist.size() - 1;
                    }
                    hardStop();
                    mediaPlayer.release();
                    mediaPlayer = MediaPlayer.create(context, playlist.get(currentSongIndex));
                }
            }
            inProcessLook = !inProcessLook;
        }
    }
}
