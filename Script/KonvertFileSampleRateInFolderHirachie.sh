#!/bin/bash
# Script iterate throuh all wav-files in folder by parameter and convert it. Needs FFmpeg
FILES=$1/*/*.wav

for f in $FILES
do
  echo "Processing $f file..."
  ffmpeg -y -i $f -ar 16000 -acodec pcm_s16le $f
done
