#!/bin/bash
# Sven
# this slows down all files in folder (parameter 1) and cut it lenght back to 1 second
# also it rename it files with the a "slow" or "fast" (parameter 2) in name

FILES=$1/*.wav

for f in $FILES
do
  echo "Processing $f file..."
  fnew=${f/_nohash_/_nohash_$2}
  ffmpeg -y -i $f -filter:a "atempo=0.7" -ss 00:00:00.20 -to 00:00:01.20 -vn -y $fnew
done
