# Rename all files in a Folder with leading word from parameter and ending number iteration
# Run in Windows like: ./script.bat aaron

:@echo off & setlocal set "Ordner=K:\" set "NameNeu=irgend etwas" pushd "%Ordner%" set /a counter=1000000 for /f "delims=" %%i in ('dir /b /ad /on 2^>nul') do call :ProcessDir "%%i" popd goto :eof :ProcessDir set /a counter+=1 if exist "%NameNeu% %counter:~-4%" goto :ProcessDir ren %1 "%NameNeu% %counter:~-4%" goto :eof
@echo off & setlocal 

# Settings
set "Ordner=K:\Data" 
set "NameNeu=%1_nohash" 

pushd "%Ordner%" 
set /a counter=1000 
for %%i in ('dir /b /ad /on 2^>nul') do call :ProcessDir "%%i" 
popd 
goto :eof 
:ProcessDir 
set /a counter+=1 
if exist "%NameNeu%_%counter:~-3%.wav" goto :ProcessDir 
ren %1 "%NameNeu%_%counter:~-3%.wav" 
goto :eof
