#!/bin/bash
# Sven
# this speed up all files in folder (parameter 1) and cut it lenght back to 1 second
# also it rename it files with the a "slow" or "fast" (parameter 2) in name

FILES=$1/*/*.wav

for f in $FILES
do
  echo "Processing $f file..."
  fnew=${f/_nohash_/_nohash_$2}
  #faster
  ffmpeg -y -i $f -filter:a "atempo=1.5" -vn -y $fnew
  #add silence before audio
  ffmpeg -y -f lavfi -t 0.5 -i anullsrc=channel_layout=stereo:sample_rate=16000 -i $fnew -filter_complex "[1:a][0:a]concat=n=2:v=0:a=1" $fnew
  #add silence after audio
  ffmpeg -y -f lavfi -t 0.5 -i anullsrc=channel_layout=stereo:sample_rate=16000 -i $fnew -filter_complex "[0:a][1:a]concat=n=2:v=0:a=1" $fnew
  #cut audi to specific length
  ffmpeg -y -i $fnew -ss 00:00:00.50 -to 00:00:01.50 -vn -y $fnew

done
